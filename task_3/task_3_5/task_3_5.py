# В массиве найти максимальный отрицательный элемент. Вывести на экран его значение и позицию в массиве.

from operator import itemgetter

list_1 = [8, -3, 15, 6, -4, 2, 8, 3, -15, 6, 4, -2]
ind, value = sorted([(number, element) for number, element in enumerate(list_1) if element < 0], key=itemgetter(1))[-1]

print(ind, value)




