# 1. В диапазоне натуральных чисел от 2 до 99 определить,
# сколько из них кратны каждому из чисел в диапазоне от 2 до 9

from pprint import pprint

pprint({value: [local_val for local_val in range(2, 10) if value % local_val == 0] for value in range(2, 100)})
