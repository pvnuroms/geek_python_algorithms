# 6. В одномерном массиве найти сумму элементов, находящихся между минимальным и максимальным элементами.
# Сами минимальный и максимальный элементы в сумму не включать.

from operator import itemgetter

list_1 = [8, -3, 15, 6, -4, 2, 8, 3, -15, 6, 4, -2]
sorted_list = sorted([(number, element) for number, element in enumerate(list_1)], key=itemgetter(1))

min_ind, min_val = sorted_list[0]
max_ind, max_val = sorted_list[-1]
min_slice_border, max_slice_border = sorted((min_ind, max_ind))
print(min_slice_border, max_slice_border)
slice = list_1[min_slice_border+1:max_slice_border]
print(slice, sum(slice))