# 4. Определить, какое число в массиве встречается чаще всего.

from operator import itemgetter

input_list = [8, 3, 15, 6, 4, 2, 8, 3, 15, 6, 4, 6]

value_dict = {}
for value in input_list:
    value_dict.setdefault(value, 0)
    value_dict[value] = value_dict[value] + 1

value_dict = sorted(value_dict.items(), key=itemgetter(1))
value, quantity = value_dict[-1]
print(f'{value} встречается {quantity} раз')