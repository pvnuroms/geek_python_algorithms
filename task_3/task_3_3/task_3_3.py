# В массиве случайных целых чисел поменять местами минимальный и максимальный элементы.

from operator import itemgetter


list_1 = [7, 1, 4, 6, 2, 8]
sorted_tuple = sorted(enumerate(list_1), key=itemgetter(1))
min_ind, min_val = sorted_tuple[0]
max_ind, max_val = sorted_tuple[-1]
list_1[min_ind] = max_val
list_1[max_ind] = min_val
print(list_1)
