# Подсчитать, сколько было выделено памяти под переменные в
# ранее разработанных программах в рамках первых трех уроков.
# Проанализировать результат и определить программы с наиболее эффективным использованием памяти.
# Примечание: Для анализа возьмите любые 1-3 ваших программы или
# несколько вариантов кода для одной и той же задачи.
# Результаты анализа вставьте в виде комментариев к коду.
# Также укажите в комментариях версию Python и разрядность вашей ОС.

# 3. Сформировать из введенного числа обратное по порядку входящих в него цифр и вывести на экран.
# Например, если введено число 3486, то надо вывести число 6843.

def get_digits(number: int) -> list:  # 136b - ссылка на объект функция
    final_list = []  # 40 - пустой список
    while number > 0:  # 24  - временная переменная 0
        final_list.append(str(number % 10))  # временная переменная 10 вероятно определяется один раз в цикле,
        # (37 +1) + 8 - добавление str в list
        number = number // 10 # 24b * (len(number) - 1) - переопределение number
    return final_list


try:
    value = int(input('введи число'))
    """
    input(?) +
    ('введи число') = 37 + 11
     (37 + len(str)) +
     int(?)
      + 24 - результат
    """
    print(''.join(get_digits(value)))
    """
    print(?) +
    join(?) + на выходе join str: 37 + len(str)
    get_digits(расчет выше)
    """
except ValueError:
    exit('не число')



from sys import getsizeof

print(getsizeof(get_digits))