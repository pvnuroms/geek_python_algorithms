# Без использования «Решета Эратосфена»;
# Используя алгоритм «Решето Эратосфена»
# Примечание ко всему домашнему заданию: Проанализировать скорость и сложность алгоритмов.
# Результаты анализа сохранить в виде комментариев в файле с кодом.

# final_value = int(input("вывод простых чисел до числа ... "))
final_value = 100


def is_simple(number: int) -> bool:
    for subnumber in range(2, number):
        if number % subnumber == 0:
            return False
    return True


def simple_dimple(final_value: int = final_value) -> set:
    """

    O(N^2)
    """
    result = {num for num in range(1, final_value) if is_simple(num)}
    result.remove(1)
    return result


def eratosfen(final_value: int = final_value) -> list:

    """
    O(N*log(log(n)))
    """

    digit_list = list(range(final_value))  # создание массива с n количеством элементов
    # вторым элементом является единица, которую не считают простым числом
    # забиваем ее нулем.
    digit_list[1] = 0
    iteration_value = 2  # замена на 0 начинается с 3-го элемента (первые два уже нули)
    while iteration_value < final_value: # перебор всех элементов до заданного числа
        if digit_list[iteration_value] != 0: # если он не равен нулю, то
            j = iteration_value * 2 # увеличить в два раза (текущий элемент - простое число)
            while j < final_value:
                digit_list[j] = 0 # заменить на 0
                j = j + iteration_value # перейти в позицию на m больше
        iteration_value += 1
    return [value for value in digit_list if digit_list[value] != 0]


if __name__ == '__main__':
    print(eratosfen(100))
    print(simple_dimple(100))
    print(is_simple(10))