import profile
import timeit
from find_simpe_dimple import simple_dimple, eratosfen

profile.run('simple_dimple(10000)')
profile.run('eratosfen(10000)')


# setup code is executed just once
mysetup = """from operator import itemgetter
from find_simpe_dimple import simple_dimple, eratosfen
"""

mycode = """
simple_dimple(10000)"""
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=2))

mycode = 'eratosfen(10000)'
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=2))



# Увеличим лист до больших значений



# 0.39216589999999996
# 0.0037850000000000383