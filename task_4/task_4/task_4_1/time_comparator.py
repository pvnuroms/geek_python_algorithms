import profile
import timeit
from task_4_1 import sum_between_extr, sum_between_extr_optimal

profile.run('sum_between_extr()')
profile.run('sum_between_extr_optimal()')


# setup code is executed just once
mysetup = """from operator import itemgetter
from task_4_1 import sum_between_extr, sum_between_extr_optimal
"""

mycode = """
sum_between_extr()"""
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=1000))

mycode = 'sum_between_extr_optimal()'
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=1000))

# 0.0018170000000000061
# 0.0014233999999999983

# Увеличим лист до больших значений

mycode = """
sum_between_extr(list(range(50000)))"""
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=1000))

mycode = 'sum_between_extr_optimal(list(range(50000)))'
print(timeit.timeit(setup=mysetup,
                    stmt=mycode,
                    number=1000))

# 6.5764447
# 4.646897200000001