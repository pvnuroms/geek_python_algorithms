# 1. Проанализировать скорость и сложность одного любого алгоритма, разработанных в
# рамках практического задания первых трех уроков.
# Примечание: попробуйте написать несколько реализаций алгоритма и сравнить их.

# 6. В одномерном массиве найти сумму элементов, находящихся между минимальным и максимальным элементами.
# Сами минимальный и максимальный элементы в сумму не включать.


from operator import itemgetter

list_1 = [8, -3, 15, 6, -4, 2, 8, 3, -15, 6, 4, -2]


def sum_between_extr(input_list: list = list_1):
    # O(NLogN) - Алгоритмическая сложность сортировки
    sorted_list = sorted([(number, element) for number, element in enumerate(input_list)], key=itemgetter(1))

    # Получение по индексу O(1)

    min_ind, min_val = sorted_list[0]
    max_ind, max_val = sorted_list[-1]

    # Сортировкой кортежа из 2 элементов можно пренебречь

    min_slice_border, max_slice_border = sorted((min_ind, max_ind))

    # Насколько я понимаю получение среза в худшем случае O(N)

    slice = list_1[min_slice_border + 1:max_slice_border]
    return slice, sum(slice)


"""
    # Итого:
    # O(NLogN) + O(N)
    # Вывод от сортировки при оптимизации можно отказаться и сделать в один проход по элементам листа получение максимального и минимального элемента
    Делаем оптимизацию
"""


def sum_between_extr_optimal(input_list: list = list_1):
    max_val, min_val = None, None
    for number, element in enumerate(input_list):
        if not max_val:
            max_val = min_val = number, element
        elif element > max_val[1]:
            max_val = number, element
        elif element < min_val[1]:
            min_val = number, element
    min_ind, min_val = min_val
    max_ind, max_val = max_val
    min_slice_border, max_slice_border = sorted((min_ind, max_ind))
    slice = list_1[min_slice_border + 1:max_slice_border]
    return slice, sum(slice)


print(sum_between_extr())
print(sum_between_extr_optimal())

