# Вводятся три разных числа. Найти, какое из них является средним (больше одного, но меньше другого).

import re

digit_list = sorted([int(value) for value in re.findall('\d+', input('Покажу фокус, введи три разных числа'))])
if not len(set(digit_list)) == 3:
    exit('У вас не получилось ввести три разных числа')
print(digit_list[1])