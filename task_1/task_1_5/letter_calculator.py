# 5. Пользователь вводит две буквы. Определить, на каких местах алфавита они стоят и сколько между ними находится букв.

import re
from string import ascii_lowercase
from pprint import pprint

try:
    letters = re.findall('[a-z]{1}', inp_value := input('Покажу фокус, введи две незаглавные латинские буквы'))[:2]
    print(letters)
    if len(letters) != 2:
        raise ValueError
except ValueError:
    print(f'Ух, {inp_value}? серьезно?')
    exit()

letter_info = ((letters[0], ascii_lowercase.index(letters[0]) + 1), (letters[1], ascii_lowercase.index(letters[1]) + 1))

pprint(f'буковка 1: {letter_info[0][0]}, {letter_info[0][1]}')
pprint(f'буковка 2: {letter_info[1][0]}, {letter_info[1][1]}')
pprint(f'Между ними {abs(letter_info[0][1]-letter_info[1][1])} буков')
