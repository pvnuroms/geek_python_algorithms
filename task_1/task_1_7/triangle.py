# 7. По длинам трех отрезков, введенных пользователем, определить возможность существования треугольника,
# составленного из этих отрезков. Если такой треугольник существует,
# то определить, является ли он разносторонним, равнобедренным или равносторонним.

from typing import Optional


class Triangle:

    __slots__ = ["sides", "is_possible"]

    def __init__(self, input_text: Optional[str] = None):

        if not input_text:
            input_text = input('Введи через пробел длины сторон треугольника')
        self.sides = [int(side) for side in input_text.split() if side.isdigit() and int(side) > 0][:3]
        self.sides = self.sides if len(self.sides) == 3 else []
        self.is_possible = self.sides != [] and (sum(self.sides) - 2*max(self.sides) > 0)

    @property
    def self_type(self) -> Optional[str]:
        if not self.is_possible:
            return None
        elif len(set(self.sides)) == 1:
            return 'Равносторонний'
        elif len(set(self.sides)) == 2:
            return 'Равнобедренный'
        return 'Обычный'

if __name__ == '__main__':
    triangle = Triangle()
    print('Существует: ', triangle.is_possible)
    if triangle.is_possible:
        print(triangle.self_type)
