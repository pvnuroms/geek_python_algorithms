import unittest
from triangle import Triangle


class TriangleTest(unittest.TestCase):

    def test_is_possible(self):
        self.triangle = Triangle('0 1 3')
        self.assertEqual(self.triangle.is_possible, False)
        self.triangle = Triangle('100 1 3')
        self.assertEqual(self.triangle.is_possible, False)
        self.triangle = Triangle('5 4 3')
        self.assertEqual(self.triangle.is_possible, True)

    def test_type(self):
        self.triangle = Triangle('1 3 3')
        self.assertEqual(self.triangle.self_type, 'Равнобедренный')
        self.triangle = Triangle('1 1 1')
        self.assertEqual(self.triangle.self_type, 'Равносторонний')
        self.triangle = Triangle('5 4 3')
        self.assertEqual(self.triangle.self_type, 'Обычный')
        self.triangle = Triangle('70 2 3')
        self.assertEqual(self.triangle.self_type, None)

