# 4. Написать программу, которая генерирует в указанных пользователем границах:
# случайное целое число;
# случайное вещественное число;
# случайный символ.
# Для каждого из трех случаев пользователь задает свои границы диапазона.
# Например, если надо получить случайный символ от 'a' до 'f', то вводятся эти символы.
# Программа должна вывести на экран любой символ алфавита от 'a' до 'f' включительно.

from random import randint
from typing import Union, Optional, Tuple
from pydantic import BaseModel, Field, ValidationError
import re

TEXT_TEMPLATE1 ="""
        Введите:
        1 - для получения случайного целого числа
        2 - для получения случайного вещественного числа
        3 - для получения случайной латинской буквы
        """

TEXT_TEMPLATE2 ="""
        А теперь без резких движений набери через пробел минимальное и максимальное значение
        """


class Value(BaseModel):
    value_type_selector: int = Field(..., gt=0, lt=4)


class ValueGenerator:
    def __init__(self):
        self.value_type = None
        self.scope = None

    def type_validation(self, input_string: Optional[str] = None) -> Union[bool, str]:
        try:
            if not input_string:
                input_string = input(TEXT_TEMPLATE1)
            _ = Value(value_type_selector=int(input_string) if input_string.isdigit() else 300)
        except ValidationError:
            return 'Ты запутался, человек'
        self.value_type = int(input_string)
        return True

    def scope_generator(self, input_string: Optional[str] = None) -> bool:
        try:
            if not input_string:
                input_string = input(TEXT_TEMPLATE2)
            if self.value_type == 1:
                self.scope = [int(digit) for digit in input_string.split()][:2]
            elif self.value_type == 2:
                self.scope = [float(digit) for digit in re.findall('\d+.\d+', input_string)[:2]]
            elif self.value_type == 3:
                self.scope = [ord(symbol) for symbol in re.findall('\D{1}', input_string)[:2]]
            self.scope.sort()
            if len(self.scope) != 2:
                raise ValueError
        except ValueError:
            return False
        except TypeError:
            return False
        return True

    def printer(self):
        if not self.value_type or not self.scope:
            return 'Не валяй дурака, вводи нормальные значения'
        if self.value_type == 1:
            return randint(self.scope[0], self.scope[1])
        if self.value_type == 2:
            return randint(self.scope[0]*10, self.scope[1]*10) / 10
        elif self.value_type == 3:
            return chr(randint(self.scope[0], self.scope[1]))

if __name__ == '__main__':
    my_gen = ValueGenerator()
    my_gen.type_validation()
    my_gen.scope_generator()
    print(my_gen.printer())