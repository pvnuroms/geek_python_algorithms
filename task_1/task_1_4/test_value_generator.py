import unittest
from value_generator import ValueGenerator


class CalculatingTest(unittest.TestCase):

    def setUp(self):
        self.my_little_value_generator = ValueGenerator()

    def test_type_validation(self):
        self.assertEqual(self.my_little_value_generator.type_validation('0'), 'Ты запутался, человек')
        self.assertEqual(self.my_little_value_generator.type_validation('1'), True)
        self.assertEqual(self.my_little_value_generator.type_validation('gg'), 'Ты запутался, человек')
        self.assertEqual(self.my_little_value_generator.type_validation('4'), 'Ты запутался, человек')
        self.assertEqual(self.my_little_value_generator.type_validation('0'), 'Ты запутался, человек')

    def test_value_validation_int(self):
        self.my_little_value_generator.type_validation('1')
        self.my_little_value_generator.scope_generator('1 100')
        self.assertIsInstance(self.my_little_value_generator.printer(), int, 'Не число')
        self.assertLessEqual(self.my_little_value_generator.printer(), 100, 'Вне диапазона')
        self.assertGreaterEqual(self.my_little_value_generator.printer(), 1, 'Вне диапазона')

    def test_value_validation_int_fail(self):
        self.my_little_value_generator.type_validation('1')
        self.my_little_value_generator.scope_generator('1.0 kk')
        self.assertEqual(self.my_little_value_generator.printer(), 'Не валяй дурака, вводи нормальные значения')

    def test_value_validation_float(self):
        self.my_little_value_generator.type_validation('2')
        self.my_little_value_generator.scope_generator('1.0 100.1')
        self.assertIsInstance(self.my_little_value_generator.printer(), float, 'Не число')
        self.assertLessEqual(self.my_little_value_generator.printer(), 100, 'Вне диапазона')
        self.assertGreaterEqual(self.my_little_value_generator.printer(), 1, 'Вне диапазона')

    def test_value_validation_float_fail(self):
        self.my_little_value_generator.type_validation('2')
        self.my_little_value_generator.scope_generator('1 100')
        self.assertEqual(self.my_little_value_generator.printer(), 'Не валяй дурака, вводи нормальные значения')

    def test_value_validation_str(self):
        self.my_little_value_generator.type_validation('3')
        self.my_little_value_generator.scope_generator('a z')
        self.assertIsInstance(self.my_little_value_generator.printer(), str, 'Не строка')
        self.assertEqual(len(self.my_little_value_generator.printer()), 1, 'Не буква')

    def test_value_validation_str_fail(self):
        self.my_little_value_generator.type_validation('3')
        self.my_little_value_generator.scope_generator('1 100')
        self.assertEqual(self.my_little_value_generator.printer(), 'Не валяй дурака, вводи нормальные значения')

