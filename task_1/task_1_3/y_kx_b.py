# 3. По введенным пользователем координатам двух точек вывести уравнение прямой вида y=kx+b, проходящей через эти точки.

from typing import Optional


class Equation:

    def __init__(self, test_data: Optional[tuple] = None):
        self.input_data = test_data
        if not test_data:
            print('Введи координаты своими кожаными пальцами')
            self.input_data = input('x1?'), input('y1?'), input('x2?'), input('y2?')

    @staticmethod
    def __local_validation(string: str) -> bool:
        return string.isdigit()

    @property
    def validation(self) -> bool:
        return all({val.lstrip('-').isdigit() for val in set(self.input_data)}) and not self.input_data[:2] == self.input_data[2:]

    def calculation(self):
        while not self.validation:
            print('Соберись и введи четыре числа своими кожаными пальцами')
            self.input_data = input('x1?'), input('y1?'), input('x2?'), input('y2?')
        self.input_data = map(int, self.input_data)
        x1, y1, x2, y2 = self.input_data
        if x1 == x2:
            return f'y = {y1 - y2}'
        k = (y1 - y2) / (x1 - x2)
        b = y2 - k*x2
        if y1 == y2:
            return f'y = b'
        return f'y = {k}x + {b}' if b > 0 else f'y = {k}x - {-1*b}'


if __name__ == '__main__':
    main = Equation()
    print(main.calculation())


