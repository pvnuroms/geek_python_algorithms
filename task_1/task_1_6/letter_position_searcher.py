# 5. Пользователь вводит номер буквы в алфавите. Определить, какая это буква.

from string import ascii_lowercase

try:
    number = input('Какую букву найти, просто дай ее номер')
    if not number.isdigit() or int(number) > 26:
        raise ValueError
except ValueError:
    print(f'Ух, {number}? серьезно?')
    exit()
number = int(number)-1
print(f'{number+1}ая буква - это {ascii_lowercase[number]}, стыдно не знать человек')


