# 8. Определить, является ли год, который ввел пользователем, високосным или невисокосным.

# год, номер которого кратен 400, — високосный;
# остальные годы, номер которых кратен 100, — невисокосные (например, годы 1700, 1800, 1900, 2100, 2200, 2300);
# остальные годы, номер которых кратен 4, — високосные[5].
# все остальные годы — невисокосные.
# Расчеты для н.э

from typing import Optional


class Year:

    def __init__(self, text_value: Optional[str] = None):
        self.year = self.input_year(text_value)

    @staticmethod
    def input_year(text_value: Optional[str] = None):
        if not text_value:
            text_value = input('Введите год')
        return int(text_value) if text_value.isdigit() else exit('Сам считай тогда')

    @property
    def is_leap(self):
        if self.year % 400 == 0:
            return True
        elif self.year % 100 == 0:
            return False
        elif self.year % 4 == 0:
            return True
        return False


if __name__ == '__main__':
    year = Year()
    print(year.is_leap)