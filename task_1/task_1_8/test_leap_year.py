import unittest
from leap_year import Year


class LeapYearTest(unittest.TestCase):

    def test_is_leap_year(self):
        self.year = Year('2')
        self.assertEqual(self.year.is_leap, False)
        self.year = Year('4')
        self.assertEqual(self.year.is_leap, True)
        self.year = Year('400')
        self.assertEqual(self.year.is_leap, True)
        self.year = Year('700')
        self.assertEqual(self.year.is_leap, False)
