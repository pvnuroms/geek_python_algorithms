# 1. Найти сумму и произведение цифр трехзначного числа, которое вводит пользователь.

from pydantic import BaseModel, Field, ValidationError
from typing import Union, Tuple, Optional
from functools import reduce


class Value(BaseModel):
    value: int = Field(..., gt=99, lt=1000)


class Calculating:
    def __init__(self):
        self.value: Optional[tuple] = None

    def validation(self, input_string: Optional[str] = None) -> Union[bool, str]:
        try:
            if not input_string:
                input_string = input('Введите трехзначное число')
            if not input_string.isdigit():
                raise ValueError()
            value = Value(
                value=int(input_string)
            )
        except ValidationError:
            return 'ТРЕХЗНАЧНОЕ, пожалуйста'
        except ValueError:
            return 'ЧИСЛО, пожалуйста'
        self.value = (value.value // 100, value.value % 100 // 10, value.value % 100 % 10,)
        return True

    def __calculating_sum(self) -> int:
        return sum(self.value)

    def __calculating_multiply(self) -> int:
        return reduce(lambda x, y: x * y, self.value)

    def calculating(self) -> Tuple[int, int]:
        while not self.value:
            print(self.validation())
        return self.__calculating_sum(), self.__calculating_multiply()


if __name__ == '__main__':
    main = Calculating()
    print(main.calculating())



