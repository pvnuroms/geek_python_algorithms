import unittest
from sum_mult_of_input_digit import Calculating


class CalculatingTest(unittest.TestCase):

    def setUp(self):
        self.calculating = Calculating()

    def test_validation(self):
        self.calculating.validation('99')
        self.assertEqual(self.calculating.value, None)
        self.calculating.validation('jj')
        self.assertEqual(self.calculating.value, None)
        self.calculating.validation('1000')
        self.assertEqual(self.calculating.value, None)
        self.calculating.validation('479')
        self.assertEqual(self.calculating.value, (4, 7, 9,))

    def test_calculating(self):
        self.calculating.validation('479')
        self.assertEqual(self.calculating.calculating(), (20, 252,))
