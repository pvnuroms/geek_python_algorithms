# 1. Пользователь вводит данные о количестве предприятий, их наименования и прибыль за 4 квартала
# (т.е. 4 отдельных числа) для каждого предприятия.. Программа должна определить среднюю прибыль (за год для всех предприятий)
# и вывести наименования предприятий, чья прибыль выше среднего и отдельно вывести наименования предприятий,
# чья прибыль ниже среднего.

# Примечание: для решения задач попробуйте применить какую-нибудь коллекцию из модуля collections

from pprint import pprint
from collections import namedtuple, defaultdict
from random import randint

# test = False
test = True

if test:
    quantity = 3
    company_list = ['Coca', 'Pepsi', 'Avtovaz']


    def generate_company(num: int):
        return company_list[num], randint(0, 10), randint(0, 10), randint(0, 10), randint(0, 10)


try:
    Corporation = namedtuple('Corporation', ['name', 'q1', 'q2', 'q3', 'q4', 'year'])
    corporation_quantity = int(input('Сколько компаний?')) if not test else quantity
    company_dict = {}
    border_value = []
    for company in range(corporation_quantity):  # O(n)
        name, q1, q2, q3, q4 = input('Введи через пробел название компании и доходы поквартально').split() if not test else generate_company(company)
        corporation = Corporation(name, int(q1), int(q2), int(q3), int(q4),
                                  sum((int(q1), int(q2), int(q3), int(q4)))
                                  )  # + O(n)
        company_dict.setdefault(corporation.name, corporation)  # + O(n)
        border_value.append(corporation.year)  # + O(n)
    pprint(dict(company_dict.items()))
    border_value = sum(border_value) / corporation_quantity  # + O(n)

except TypeError:
    print('Эх, надо было нормальные данные вводить, что же ты...')

categorized_dict = defaultdict(list)
for corporation, values in company_dict.items():  # + O(n)
    if values.year > border_value:
        categorized_dict['big_companies'].append(corporation)
    else:
        categorized_dict['small_companies'].append(corporation)

print(categorized_dict)

"""
O(6*n)
"""