# Написать программу сложения и умножения двух шестнадцатеричных чисел.
# При этом каждое число представляется как массив, элементы которого это цифры числа.
# Например, пользователь ввёл A2 и C4F. Сохранить их как [‘A’, ‘2’] и [‘C’, ‘4’, ‘F’] соответственно.
# Сумма чисел из примера: [‘C’, ‘F’, ‘1’], произведение - [‘7’, ‘C’, ‘9’, ‘F’, ‘E’].

from itertools import zip_longest
from collections import deque

dict_10_16 = {str(value): number for number, value in enumerate(list(range(10)) + ['A', 'B', 'C', 'D', 'E', 'F'])}
dict_16_10 = {value: key for key, value in dict_10_16.items()}


class MaxHaveNoHex:

    def __init__(self, value: str):
        self.str = value

    def __add__(self, other):

        """
        O(a)+O(b)+O(6*max([ab]))
        """

        add_ = 0
        d = deque()
        for a, b in zip_longest(self.str[::-1], other.str[::-1], fillvalue='0'):
            result = dict_10_16.get(a) + dict_10_16.get(b) + add_
            ostatok = result % 16
            add_ = result // 16
            d.appendleft(dict_16_10.get(ostatok))
        if add_ == 1:
            d.appendleft(1)
        print(hex(int(self.str, 16) + int(other.str, 16)))  # for control
        return d

    def __mul__(self, other):
        """
        O(6*a*b)
        """

        result_fin = 0
        for razryad_a, a in enumerate(self.str[::-1]):  # + O(ab)
            for razryad_b, b in enumerate(other.str[::-1]):
                result = (dict_10_16.get(a) * (16**razryad_a)) * (dict_10_16.get(b) * (16**razryad_b))
                result_fin += result
        print(hex(result_fin))  # for control
        return self.my_hex(result_fin)

    @staticmethod
    def my_hex(digit: int) -> deque:
        final_list = deque()
        while digit > 0:
            final_list.appendleft(dict_16_10.get(digit % 16))
            digit = digit // 16
        return final_list


val1 = MaxHaveNoHex('A2')
val2 = MaxHaveNoHex('C4F')
print(val1+val2)
print(val1*val2)

val1 = MaxHaveNoHex('AAA')
val2 = MaxHaveNoHex('BBB')
print(val1+val2)
print(val1*val2)



