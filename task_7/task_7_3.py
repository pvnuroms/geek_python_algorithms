# 3. Массив размером 2m + 1, где m – натуральное число, заполнен случайным образом.
# Найдите в массиве медиану. Медианой называется элемент ряда, делящий его на две равные части:
# в одной находятся элементы, которые не меньше медианы, в другой – не больше медианы.
# Задачу можно решить без сортировки исходного массива. Но если это слишком сложно,
# то используйте метод сортировки, который не рассматривался на уроках
import random


def get_med(input_list: list) -> int:
    mediana_dict = {'most_likely': len(input_list)}
    for element in input_list:
        mediana_dict[element] = 0
        for compare_val in input_list:
            if element >= compare_val:
                mediana_dict[element] += 1
            if element <= compare_val:
                mediana_dict[element] -= 1
        # print(element, mediana_dict[element])
        if mediana_dict.get(element) == 0:
            return element
        # print(mediana_dict.get('most_likely'), abs(mediana_dict[element]))
        if abs(mediana_dict[element]) < mediana_dict.get('most_likely'):
            mediana_dict['most_likely'] = abs(mediana_dict[element])
            mediana = element
    return mediana


m = 10
random_list = [random.randint(0, 50) for _ in range(2*m+1)]
# random_list = [4, 38, 8, 41, 22, 7, 41, 39, 9, 50, 15, 25, 9, 27, 22, 27, 38, 1, 15, 1, 27]
# random_list.sort()
print(f'Исходный список длинной {len(random_list)}', random_list)
mediana = get_med(random_list)
print(f'медиана = {mediana}')

# check
lower_list = [val for val in random_list if val <= mediana]
upper_list = [val for val in random_list if val >= mediana]

print(lower_list, mediana, upper_list)
print(f'{len(lower_list)} шт=<{mediana}<={len(upper_list)} шт')

