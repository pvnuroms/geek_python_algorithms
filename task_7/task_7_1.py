# 1. Отсортируйте по убыванию методом "пузырька" одномерный целочисленный массив,
# заданный случайными числами на промежутке [-100; 100). Выведите на экран исходный и отсортированный массивы.
# Сортировка должна быть реализована в виде функции. По возможности доработайте алгоритм (сделайте его умнее).

import random


random_list = [random.randint(-100, 99) for _ in range(100)]


def bubble_sort(sorted_list: list):
    change_counter = 0
    total_iteration = 0
    is_swapped = True
    while is_swapped:
        is_swapped = False
        for _iter in range(len(sorted_list) - 1 - total_iteration):
            if sorted_list[_iter] < sorted_list[_iter + 1]:
                sorted_list[_iter + 1], sorted_list[_iter] = \
                    sorted_list[_iter], sorted_list[_iter + 1]
                change_counter += 1
                is_swapped = True
        total_iteration += 1
    return sorted_list, change_counter


def bubble_sort_up(sorted_list: list):
    factor = int(len(sorted_list) / 1.247)
    change_counter = 0
    while factor > 1:
        _iter = 0
        while _iter + factor + 1 < len(sorted_list):
            if sorted_list[_iter] < sorted_list[_iter + factor]:
                sorted_list[_iter + factor], sorted_list[_iter] = \
                    sorted_list[_iter], sorted_list[_iter + factor]
                change_counter += 1
            _iter += 1
        factor = int(factor / 1.247)
    # print('pre_sorted_listed = ', sorted_list)
    total_iteration = 0
    is_swapped = True
    while is_swapped:
        is_swapped = False
        for _iter in range(len(sorted_list) - 1 - total_iteration):
            if sorted_list[_iter] < sorted_list[_iter + 1]:
                sorted_list[_iter + 1], sorted_list[_iter] = \
                    sorted_list[_iter], sorted_list[_iter + 1]
                change_counter += 1
                is_swapped = True
        total_iteration += 1
    return sorted_list, change_counter


print(random_list)
buble_sorted_list, changes = bubble_sort(random_list[:])
print(changes, buble_sorted_list)
up_buble_sorted_list, changes = bubble_sort_up(random_list[:])
print(changes, up_buble_sorted_list)


