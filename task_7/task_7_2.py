# 2. Отсортируйте по возрастанию методом слияния одномерный вещественный массив,
# заданный случайными числами на промежутке [0; 50). Выведите на экран исходный и отсортированный массивы.

import random

random_list = [random.randint(0, 50) for _ in range(20)]

print('Исходный список', random_list)

def merge_sort(sorted_list):
    if len(sorted_list) > 1:
        mid = len(sorted_list)//2
        left_part, right_part = sorted_list[:mid], sorted_list[mid:]
        # print('сено', left_part)
        # print('солома', right_part)
        merge_sort(left_part)
        merge_sort(right_part)
        left_index = right_index = total_index = 0
        while left_index < len(left_part) and right_index < len(right_part):
            if left_part[left_index] < right_part[right_index]:
                sorted_list[total_index] = left_part[left_index]
                left_index += 1
            else:
                sorted_list[total_index] = right_part[right_index]
                right_index += 1
            total_index += 1
        while left_index < len(left_part):
            sorted_list[total_index] = left_part[left_index]
            left_index += 1
            total_index += 1
        while right_index < len(right_part):
            sorted_list[total_index] = right_part[right_index]
            right_index += 1
            total_index += 1
        # print(sorted_list)
        return sorted_list


print('Отсортированный список', merge_sort(random_list))
