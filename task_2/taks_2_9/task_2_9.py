# 9. Среди натуральных чисел, которые были введены, найти наибольшее по сумме цифр.
# Вывести на экран это число и сумму его цифр.


from operator import itemgetter


def get_sum_of_digits(number: int) -> set:
    final_list = []
    while number > 0:
        final_list.append(number % 10)
        number = number // 10
    return sum(final_list[::-1])


try:
    value_list = [(int(digit), get_sum_of_digits((int(digit)))) for digit in input('введи последовательность чисел').split() if digit.isdigit()]
    result_sorted = sorted(value_list, key=itemgetter(1))
    print(f'{result_sorted[-1]} чемпион в {result_sorted}')

except ValueError:
    exit('не натуральное число')