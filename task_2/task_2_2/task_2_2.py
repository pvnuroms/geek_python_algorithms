# 2. Посчитать четные и нечетные цифры введенного натурального числа.
# Например, если введено число 34560,
# то у него 3 четные цифры (4, 6 и 0) и 2 нечетные (3 и 5).


def get_digits(number: int) -> set:
    final_list = []
    while number > 0:
        final_list.append(number%10)
        number = number // 10
    return set(final_list[::-1])


def is_even(number: int) -> bool:
    return number % 2 == 0


try:
    value = int(input('введи число'))
    all_list = get_digits(value)
    even_list = {value for value in all_list if is_even(value)}
    not_even_list = all_list - even_list
    print(f'четные:{even_list}\nнечетные:{not_even_list}')

except ValueError:
    exit('не натуральное число')

