# 7. Напишите программу, доказывающую или проверяющую,
# что для множества натуральных чисел выполняется равенство: 1+2+...+n = n(n+1)/2, где n - любое натуральное число.


class EternalCalculator:

    def __init__(self):
        self.iteration = 0
        self.sum = 0
        self.multi_ = 0


    def next_iter(self):
        self.iteration += 1
        self.sum += self.iteration
        self.multi_ = self.iteration*(self.iteration+1)/2

    def some_check(self):
        while self.sum == self.multi_:
            print(f'для {self.iteration} пока правда {self.sum}={self.multi_}, но давай проверим еще')
            self.next_iter()
        else:
            print(f'для {self.iteration} неправда {self.sum}={self.multi_}')


if __name__ == '__main__':
    game = EternalCalculator()
    game.some_check()
