# 3. Сформировать из введенного числа обратное по порядку входящих в него цифр и вывести на экран.
# Например, если введено число 3486, то надо вывести число 6843.

def get_digits(number: int) -> list:
    final_list = []
    while number > 0:
        final_list.append(str(number % 10))
        number = number // 10
    return final_list


try:
    value = int(input('введи число'))
    print(''.join(get_digits(value)))

except ValueError:
    exit('не число')