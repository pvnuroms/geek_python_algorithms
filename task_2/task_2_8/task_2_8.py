# 8. Посчитать, сколько раз встречается определенная цифра в введенной последовательности чисел.
# Количество вводимых чисел и цифра, которую необходимо посчитать, задаются вводом с клавиатуры.


try:
    value_list = ([int(digit) for digit in input('введи последовательность чисел').split() if digit.isdigit()])
    value_dict = {}
    for value in value_list:
        value_dict.setdefault(value, 0)
        value_dict[value] = value_dict[value] + 1
    print(value_dict)
    value = int(input('введи число'))
    print(value_dict.get(value))

except ValueError:
    exit('не число')