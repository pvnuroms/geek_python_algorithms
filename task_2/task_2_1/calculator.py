# 1. Написать программу, которая будет складывать, вычитать, умножать или делить два числа.
# Числа и знак операции вводятся пользователем. После выполнения вычисления программа не должна завершаться,
# а должна запрашивать новые данные для вычислений.
# Завершение программы должно выполняться при вводе символа '0' в качестве знака операции.
# Если пользователь вводит неверный знак (не '0', '+', '-', '*', '/'),
# то программа должна сообщать ему об ошибке и снова запрашивать знак операции.
# Также сообщать пользователю о невозможности деления на ноль, если он ввел 0 в качестве делителя.

from typing import Union


class Calculator:
    def __init__(self):
        self.input_string = self.value_input()
        self.values = None
        self.is_valid = self.validate()

    @staticmethod
    def value_input() -> str:
        return input('Что посчитать?')

    def validate(self) -> Union[bool, ValueError]:
        if len(input_list := self.input_string.split()) != 3:
            return ValueError('некорректный формат ввода, давай еще раз')
        if not(input_list[0].isdigit() and input_list[-1].isdigit()):
            return ValueError('я не понял, что это за числа')
        if input_list[1] not in ('0', '+', '-', '*', '/'):
            return ValueError('я не понял, что за оператор')
        elif input_list[1] == '0':
            return False
        self.values = (int(input_list[0]), input_list[1], int(input_list[2]))
        return True

    def calculate(self):
        if self.is_valid:
            if self.values[1] == '+':
                return self.values[0] + self.values[2]
            elif self.values[1] == '-':
                return self.values[0] - self.values[2]
            elif self.values[1] == '*':
                return self.values[0] * self.values[2]
            elif self.values[1] == '/':
                return self.values[0] / self.values[2] if self.values[2] != 0 else 'себя на ноль подели, кожаный мешок'


if __name__ == '__main__':
    while True:
        calc = Calculator()
        if calc.is_valid is False:
            print('Пока')
            break
        if isinstance(calc.is_valid, ValueError):
            print(f'Ошибка: {calc.is_valid}')
            continue
        print(calc.calculate())

