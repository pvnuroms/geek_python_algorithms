# 6. В программе генерируется случайное целое число от 0 до 100.
# Пользователь должен его отгадать не более чем за 10 попыток.
# После каждой неудачной попытки должно сообщаться больше или меньше введенное пользователем число, чем то, что загадано.
# Если за 10 попыток число не отгадано, то вывести загаданное число.

from random import randint
from typing import Tuple

class SuperGame:

    def __init__(self):
        self.tries = 10
        self.goal = randint(0, 100)

    @property
    def to_try(self) -> Tuple[bool, str]:
        self.tries -= 1
        input_val = input('Введите число от 0 до 100')
        if not input_val.isdigit():
            return False, 'Не число даже'
        return self.check(int(input_val))

    def check(self, value: int) -> Tuple[bool, str]:
        if value < self.goal:
            return False, 'Слишком маленькое число'
        elif value > self.goal:
            return False, 'Слишком большое число'
        else:
            return True, f'Угадал {self.goal}'


if __name__ == '__main__':
    game = SuperGame()
    print(vars(game))
    while game.tries > 0:
        print(f'Осталось {game.tries} попыток')
        result, text = game.to_try
        print(text)
        if result:
            break
    else:
        print('Кожаный мешок проиграл')

