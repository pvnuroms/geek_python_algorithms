# 4. Найти сумму n элементов следующего ряда чисел: 1 -0.5 0.25 -0.125 ...
# Количество элементов (n) вводится с клавиатуры.

try:
    n = int(input('введи число'))
    element = 1
    summ = 1
    while n > 1:
        element = element/-2
        summ += element
        n -= 1
    print(summ)

except ValueError:
    exit('не число')